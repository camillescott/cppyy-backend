diff --git a/src/core/metacling/src/TClingCallFunc.cxx b/src/core/metacling/src/TClingCallFunc.cxx
index 48734086ce..2996bcae91 100644
--- a/src/core/metacling/src/TClingCallFunc.cxx
+++ b/src/core/metacling/src/TClingCallFunc.cxx
@@ -388,6 +391,8 @@ void TClingCallFunc::make_narg_ctor(const unsigned N, ostringstream &typedefbuf,
    callbuf << ")";
 }
 
+static std::map<OverloadedOperatorKind, std::string> binOperatorKinds;
+
 void TClingCallFunc::make_narg_call(const std::string &return_type, const unsigned N, ostringstream &typedefbuf,
                                     ostringstream &callbuf, const string &class_name, int indent_level)
 {
@@ -398,6 +403,47 @@ void TClingCallFunc::make_narg_call(const std::string &return_type, const unsign
    //
    const FunctionDecl *FD = GetDecl();
 
+   if (binOperatorKinds.empty()) {
+      // Prepare lookup map for specific binary operators that are supposed to
+      // be left in-place for Cling to resolve.
+      binOperatorKinds[OO_Plus]                   = " + ";
+      binOperatorKinds[OO_Minus]                  = " - ";
+      binOperatorKinds[OO_EqualEqual]             = " == ";
+      binOperatorKinds[OO_Star]                   = " * ";
+      binOperatorKinds[OO_Slash]                  = " / ";
+      binOperatorKinds[OO_Percent]                = " % ";
+      binOperatorKinds[OO_Caret]                  = " ^ ";
+      binOperatorKinds[OO_Amp]                    = " & ";
+      binOperatorKinds[OO_Pipe]                   = " | ";
+      binOperatorKinds[OO_Tilde]                  = " ~ ";
+      binOperatorKinds[OO_Exclaim]                = " ! ";
+      binOperatorKinds[OO_Equal]                  = " = ";
+      binOperatorKinds[OO_Less]                   = " < ";
+      binOperatorKinds[OO_Greater]                = " > ";
+      binOperatorKinds[OO_LessLess]               = " << ";
+      binOperatorKinds[OO_GreaterGreater]         = " >> ";
+      binOperatorKinds[OO_LessLessEqual]          = " <<= ";
+      binOperatorKinds[OO_GreaterGreaterEqual]    = " >>= ";
+      binOperatorKinds[OO_EqualEqual]             = " == ";
+      binOperatorKinds[OO_ExclaimEqual]           = " != ";
+      binOperatorKinds[OO_LessEqual]              = " <= ";
+      binOperatorKinds[OO_GreaterEqual]           = " >= ";
+      binOperatorKinds[OO_AmpAmp]                 = " && ";
+      binOperatorKinds[OO_PipePipe]               = " || ";
+   }
+
+   // Filter out binary operators and replace them simply by that operator
+   // to make Cling do the overload resolution. This is mainly for templates,
+   // some of which won't compile under Clang5, and others which do not have
+   // the proper specialization, even though the argument types match. (It's
+   // too late for silent SFINAE at this point.)
+   std::string optype = "";
+   if (N == 2 && FD->getDeclName().getNameKind() == DeclarationName::CXXOperatorName) {
+       auto res = binOperatorKinds.find(FD->getDeclName().getCXXOverloadedOperator());
+       if (res != binOperatorKinds.end())
+           optype = res->second;
+   }
+
    // Sometimes it's necessary that we cast the function we want to call first
    // to its explicit function type before calling it. This is supposed to prevent
    // that we accidentially ending up in a function that is not the one we're
@@ -409,7 +455,8 @@ void TClingCallFunc::make_narg_call(const std::string &return_type, const unsign
    // we supply the object parameter.
    // Therefore we only use it in cases where we know it works and set this variable
    // to true when we do.
-   bool ShouldCastFunction = !isa<CXXMethodDecl>(FD) && N == FD->getNumParams() \
+   bool ShouldCastFunction = optype.empty() && \
+                             !isa<CXXMethodDecl>(FD) && N == FD->getNumParams() \
                              && !FD->isTemplateInstantiation() && return_type != "(lambda)";
    if (ShouldCastFunction) {
       callbuf << "((" << return_type << " (&)(";
@@ -437,26 +484,29 @@ void TClingCallFunc::make_narg_call(const std::string &return_type, const unsign
       callbuf << "))";
    }
 
-   if (const CXXMethodDecl *MD = dyn_cast<CXXMethodDecl>(FD)) {
-      // This is a class, struct, or union member.
-      if (MD->isConst())
-         callbuf << "((const " << class_name << "*)obj)->";
-      else
-         callbuf << "((" << class_name << "*)obj)->";
-   } else if (const NamedDecl *ND =
-                 dyn_cast<NamedDecl>(FD->getDeclContext())) {
-      // This is a namespace member.
-      (void) ND;
-      callbuf << class_name << "::";
-   }
-   //   callbuf << fMethod->Name() << "(";
+   std::string function_name;
    {
-      std::string name;
+      llvm::raw_string_ostream stream(function_name);
+      FD->getNameForDiagnostic(stream, FD->getASTContext().getPrintingPolicy(), /*Qualified=*/false);
+   }
+
+   if (optype.empty()) {
+      if (const CXXMethodDecl *MD = dyn_cast<CXXMethodDecl>(FD)) {
+         // This is a class, struct, or union member.
+         if (MD->isConst())
+            callbuf << "((const " << class_name << "*)obj)->";
+         else
+            callbuf << "((" << class_name << "*)obj)->";
+      } else if (const NamedDecl *ND =
+                    dyn_cast<NamedDecl>(FD->getDeclContext())) {
+         // This is a namespace member.
+         (void) ND;
+         callbuf << class_name << "::";
+      }
+      //   callbuf << fMethod->Name() << "(";
       {
-         llvm::raw_string_ostream stream(name);
-         FD->getNameForDiagnostic(stream, FD->getASTContext().getPrintingPolicy(), /*Qualified=*/false);
+         callbuf << function_name;
       }
-      callbuf << name;
    }
    if (ShouldCastFunction) callbuf << ")";
 
@@ -471,14 +516,18 @@ void TClingCallFunc::make_narg_call(const std::string &return_type, const unsign
       collect_type_info(QT, typedefbuf, callbuf, type_name, refType, isPointer, indent_level, true);
 
       if (i) {
-         callbuf << ',';
-         if (i % 2) {
-            callbuf << ' ';
-         } else {
-            callbuf << "\n";
-            for (int j = 0; j <= indent_level; ++j) {
-               callbuf << kIndentString;
+         if (optype.empty()) {
+            callbuf << ',';
+            if (i % 2) {
+               callbuf << ' ';
+            } else {
+               callbuf << "\n";
+               for (int j = 0; j <= indent_level; ++j) {
+                  callbuf << kIndentString;
+               }
             }
+         } else {
+            callbuf << optype;
          }
       }
 
